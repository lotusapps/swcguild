function onPlayClick(){
	
	var startingAmount = document.getElementById("start").value;
	var currentAmount = startingAmount; //track current amount 
	var amountOfRolls = 0;	//Number of rolls until game ends
	var maxAmount = startingAmount;	//Highest Amount held
	var maxRoll = 0; //roll count on most money
	//var numOnRoll = "Rolls."	
	var luckyTime = 0; //number of times 7 was rolled
				
	if	(isNaN(startingAmount) || startingAmount == "" || startingAmount <= "0")
		{
			console.log("invalid amount");
			alert("Invalid amount, Please enter a proper amount.");
			document.getElementById("start").value = "";
			document.getElementById("start").focus();
		}
	else
		{
		do{
			var dice1;
			var dice2;
			var total;
							
			amountOfRolls++;
							
			dice1 = Math.floor((Math.random() * 6) + 1);
			dice2 = Math.floor((Math.random() * 6 )+ 1);
			total = dice1 + dice2;
						
			if(total == 7)
				{
					//Extra to dispay results
					//numOnRoll = numOnRoll.concat("<span id=\"seven\">" + total + "</span>" + ".");
					currentAmount += 4;
					luckyTime++;
					if(currentAmount > maxAmount)
						{
							maxAmount = currentAmount;
							maxRoll = amountOfRolls;
						}
				}
			else
				{
					//numOnRoll = numOnRoll.concat(total + ".");
					currentAmount -= 1;
				}
			
				console.log(dice1 + " + " + dice2 + " = " + total);
				console.log("max amount = "  + maxAmount);
				console.log("current Amount = " + currentAmount);
				console.log("amountOfRolls = " + amountOfRolls);
				console.log("amount of roll during highest amount held " + maxRoll);
				//console.log(numOnRoll);
				//document.getElementById("rolls").innerHTML = numOnRoll;		

				
			}while(currentAmount > .99)
						
	displayResult();
	document.getElementById("bet").innerHTML = startingAmount;
	document.getElementById("totalRoll").innerHTML = amountOfRolls;
	document.getElementById("highest").innerHTML = maxAmount;
	document.getElementById("highRoll").innerHTML = maxRoll;
	document.getElementById("luckyTime").innerHTML = luckyTime;					
					}
}//End of function
			
function displayResult(){
			
	document.getElementById("result").style.display = "block";
	document.getElementById("startButton").innerHTML = "Play Again";
	document.getElementById("start").value = "";
	document.getElementById("start").focus();
}
			