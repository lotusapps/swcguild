//Contact Page Javascript

function validateEntry(){
	
	var contactName = document.getElementById("name").value;
	var contactEmail = document.getElementById("email").value;
	var contactPhone = document.getElementById("phone").value;
	var reasons = document.getElementById("ddlReason").value;
	var additionalInfo = document.getElementById("additional").value;
	
	//Get checkbox values and store in an array
	var contactDays = new Array();
	var days = document.getElementsByName("day");
	for(var i = 0; i < days.length; i++){
		if(days[i].checked == true)
			contactDays.push(days[i].value);
	}
	
	//Save radio value
	var	newVisit = document.getElementsByName("newVisit");
	var newVisitValue;
	for(var i = 0; i < newVisit.length; i++){
		if(newVisit[i].checked == true)
		{
			newVisitValue = newVisit[i].value;
		}
	}
	
	//Validate Entries
	if(contactName == ""){
		document.getElementById("validateName").innerHTML = "* Name is required.";
		//alert("Please enter your name.");
		return false;
	}
	else if(contactEmail == "" && contactPhone == "")
	{
		clearValidation();
		document.getElementById("validateEmail").innerHTML = "* Email or Phone Number is required";
		//alert("Please enter your email or a phone number.");
		return false;
	}
	else if(reasons == "other" && additionalInfo == "")
	{
		clearValidation();
		document.getElementById("validateAdditional").innerHTML = "* Please give us more additional <br>" +
																  " information on what you are inquiring about.";
		//alert("Please give us more additional information on what you are inquiring about.");
		return false;
	}
	else if(contactDays.length == 0)
	{
		clearValidation();
		document.getElementById("validateDays").innerHTML = "* What are the best days to reach you?";
		//alert("What are the best days to reach you?");
		return false;
	}
	else
	{
		clearValidation();
		alert("Thank you. We will get back to you as soon as possible!");
		return true;
	}

	
}

//Clear all validation
function clearValidation(){
	
	document.getElementById("validateName").innerHTML = "";
	document.getElementById("validateEmail").innerHTML = "";
	document.getElementById("validateAdditional").innerHTML = "";
	document.getElementById("validateDays").innerHTML = "";

}