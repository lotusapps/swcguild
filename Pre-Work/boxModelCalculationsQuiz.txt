Minh Chuong
Box Model Calculation Quiz


#div1	{
	height: 150px;
	width: 400px;
	margin: 20px;
	border: 1px solid red;
	padding 10px;
}

Total Height = 20px + 1px + 10px + 150px 10px + 1px + 20px

Total Height = 212px

-------------------------------------------------------------------------------------

Total Width = 20px + 1px + 10px + 400px 10px + 1px + 20px

Total Width = 462px

-------------------------------------------------------------------------------------

Browser Calculated Height = 1px + 10px + 150px + 10px + 1px

Browser Calculated Height = 172px

-------------------------------------------------------------------------------------

Browser Calculated Width = 1px + 10px + 400px + 10px + 1px

Browser Calculated Width = 422px